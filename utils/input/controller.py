__author__ = 'Adam Keenan'

import pygame, platform
from utils import global_vars

WINDOWS = platform.system() == "Windows"

CONTROLLERS = []
BUTTONMAPPINGS = {"A":0,"B":1,"X":2,"Y":3,"LB":4,"RB":5,"BACK":6,"START":7,"LS":8,"RS":9} if WINDOWS else {"A":11,"B":12,"X":13,"Y":14,"LB":8,"RB":9,"BACK":5,"START":4,"LS":6,"RS":7,"XBOX":10,"UP":0,"DOWN":1,"LEFT":2,"RIGHT":3}
REVERSE_BUTTONMAPPINGS = {v:k for k,v in BUTTONMAPPINGS.items()}
HATMAPPINGS = {"UP":(0,1),"DOWN":(0,-1),"LEFT":(-1,0),"RIGHT":(1,0)} if WINDOWS else {"UP":0,"DOWN":1,"LEFT":2,"RIGHT":3}
AXESMAPPINGS = {"LX":0,"LY":1,"TRIGGERS":2,"RY":3,"RX":4} if WINDOWS else {"LX":0,"LY":1,"RY":2,"RX":3,"LTRIGGER":4,"RTRIGGER":5}
AXISDIRECTIONS = {"LX":1,"LY":-1,"RX":1,"RY":-1}    # Entered for RIGHT/UP
VALUES = []

def initialize_controllers():
    """
    Finds all the controllers currently known and initializes them and puts them in a list.
    """
    pygame.init()
    global CONTROLLERS
    CONTROLLERS = [pygame.joystick.Joystick(x) for x in range(pygame.joystick.get_count())]
    for joystick in CONTROLLERS:
        joystick.init()
        VALUES.append({"BUTTONS":[None]*joystick.get_numbuttons(),"AXES":[None]*joystick.get_numaxes(),"HATS":[None]*joystick.get_numhats()})
    print("Found {} controllers".format(get_count()))

def get_count():
    return pygame.joystick.get_count()

def get_count_initialized():
    return len(CONTROLLERS)

def get_controller(ID):
    return CONTROLLERS[ID]

def gather_input():
    pygame.event.pump()
    for controller in range(get_count()):
        for button_num in range(get_controller(controller).get_numbuttons()):
            VALUES[controller]["BUTTONS"][button_num] = get_controller(controller).get_button(button_num)
        for axis_num in range(get_controller(controller).get_numaxes()):
            val = get_controller(controller).get_axis(axis_num)
            VALUES[controller]["AXES"][axis_num] = val if abs(val) > global_vars.AXIS_THRESHOLD else 0
        if WINDOWS:
            for hat_num in range(get_controller(controller).get_numhats()):
                VALUES[controller]["HATS"][hat_num] = get_controller(controller).get_hat(hat_num)


def rescan():
    pygame.joystick.quit()
    pygame.joystick.init()

def get_hat_diagonal(first, second):
    """
    Pass in two HATMAPPINGS to get the combination
    """
    return tuple(sum(x) for x in zip(HATMAPPINGS[first], HATMAPPINGS[second]))