__author__ = 'Adam Keenan'

import pygame

KEYS = {pygame.KEYDOWN:{}, pygame.KEYUP:{}}

def gather_input():
    """
    Gets the current state of input and stores it in a dictionary
    """
    for type in KEYS:
        for key in KEYS[type]:
            KEYS[type][key] = False

    for event in pygame.event.get():
        if event.type in [pygame.KEYDOWN, pygame.KEYUP]:
            KEYS[event.type][event.key] = True

def is_key_down(key):
    """
    @type key: int
    """
    return KEYS[pygame.KEYDOWN][key]
