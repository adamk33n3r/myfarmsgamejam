__author__ = 'Adam Keenan'
import random
import socket
import select

class Client(object):
    """
    Adapted from http://codepad.org/e6pwGj24
    """

    def __init__(self, addr="127.0.0.1", serverport=1649):
        self.clientport = random.randrange(8000, 8999)
        self.conn = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        # Bind to localhost - set to external ip to connect from other computers
        self.conn.bind(("0.0.0.0", self.clientport))
        self.conn.setblocking(0)
        self.addr = addr
        self.serverport = serverport

        self.read_list = [self.conn]
        self.write_list = []

        # Initialize connection to server
        self.conn.sendto("c".encode("utf-8"), (self.addr, self.serverport))

    def get_messages(self):
        try:
            # select on specified file descriptors
            readable, writable, exceptional = (
                select.select(self.read_list, self.write_list, [], 0)
            )
            for disc in readable:
                if disc is self.conn:
                    msg="".encode("utf-8")
                    try:
                        while True:
                            msg, addr = self.conn.recvfrom(2048)
                    except:
                        pass
                    finally:
                        # self.conn.setblocking(1)
                        msg = msg.decode("utf-8")
                        if msg == "": # This would happen if it never got a message this call. Like if the first time recvfrom was called it errored.
                            # Shouldnt need this anymore....but just in case
                            print("Got empty message??!?!?!")
                            return
                        list_of_players=[]
                        for pos in msg.split('|'):
                            x, y, animation, hide_time, goal_x,goal_y,position, winning_status, port = pos.split(',')
                            list_of_players.append((x,y,animation, hide_time,goal_x,goal_y,position, winning_status, port))
                        return list_of_players
        except Exception as e:
            print(e)
    def quit(self):
        print("Disconnecting...")
        self.conn.sendto("d".encode('utf-8'), (self.addr, self.serverport))

    def send_command(self, command):
        self.conn.sendto(command.encode('utf-8'), (self.addr, self.serverport))
