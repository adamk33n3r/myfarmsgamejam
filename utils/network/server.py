__author__ = 'Adam Keenan'
import socket
import select
import sys, time

from utils.physics import Physics
from utils.objects import Sprite, Object
from utils import global_vars, loader


class Server(object):
    """
    Adapted from http://codepad.org/p9lVrmqn

    msgs look like this:
        [c|u|d][u|d|l|r]
    """

    def __init__(self, port=1649):
        # Connection setup
        print("Server starting up...")
        self.listener = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.listener.bind(("0.0.0.0", port))

        self.read_list = [self.listener]
        self.write_list = []

        #Game logic setup
        self.players = {}
        self.physics = Physics(1 / 30)
        self.physics_data = {}

        self.goal = None

        self.it = None
        self.runner = None

        self.won = False
        self.tagged = False


    def make_objects(self):
        ground_level1 = 0, 1260, 650
        ground_level2 = 3100, 4000 - 3100, 650
        pit_level = 1630, 2775 - 1630, 115
        invisible_ground = [ground_level1, ground_level2, pit_level]
        for ground in invisible_ground:
            Object(ground[0] + ground[1] // 2, ground[2] + 10, ground[1], 20, True, physics=True)
        # Object(100, 820, 200, 20, static=True, physics=True)
        # Object(300, 800, 200, 20, True, physics=True)
        # Object(500, 800, 200, 20, True, physics=True)
        #
        # Object(1250, 600, 200, 20, True, physics=True)
        # Object(1350, 500, 200, 20, True, physics=True)
        # Object(1450, 400, 200, 20, True, physics=True)
        # o=Object(1550, 300, 200, 20, True, physics=True)
        # return o
        list_of_platforms = loader.load_server("assets/enviro/platform_list.txt")
        import random

        rand = random.randrange(len(list_of_platforms))
        plat = list_of_platforms[rand]
        # self.goal = Object(plat.rect.x, plat.rect.y, 200, 400, True, physics=True)
        self.goal = Object(0, 900, 200, 400, True, physics=True)


    def get_messages(self):
        try:
            list_of_updates = []
            readable, writable, exceptional = (select.select(self.read_list, self.write_list, [], 0.01))
            for disc in readable:
                if disc is self.listener:
                    msg, addr = disc.recvfrom(32)
                    msg = msg.decode('utf-8')
                    if len(msg) >= 1:
                        cmd = msg[0]
                        if cmd == "c":
                            if len(self.players) < global_vars.MAX_CONNECTIONS:
                                print("New player connected:", addr)
                                self.players[addr] = (100, 1000, "stand-right", addr[1])
                                self.add_new_player(addr[1])
                        elif cmd == "i":
                            if len(msg) >= 2 and addr in self.players:
                                list_of_updates.append((msg[1:], addr[1]))
                        elif cmd == "d":
                            print(msg, cmd)
                            if addr in self.players:
                                del self.players[addr]
                                print("Player disconnected:", addr)
                        else:
                            print("Unexpected command: {}".format(msg))
            return list_of_updates
        except Exception as e:
            print("Oh no! The server crashed for some reason!")
            print("Exception:", e)
            self.listener.close()
            sys.exit(1)

    def send_messages(self, win_status=0):
        for player in self.players:
            send = []
            for pos in self.players:
                # send.append("{},{},{},{}".format(*self.players[pos]+(pos[1],)))
                p = tuple(self.physics_data[pos[1]].shape.body.position)
                send.append("{},{},{},{},{},{},{},{},{}".format(*p + (
                self.physics_data[pos[1]].current_animation, self.physics_data[pos[1]].time_at_last_hide,
                self.goal.shape.body.position.x, self.goal.shape.body.position.y,
                # 0,900,
                self.it == self.physics_data[pos[1]],
                win_status, pos[1])))
            self.listener.sendto('|'.join(send).encode("utf-8"), player)

    def add_new_player(self, port):
        width = 2000
        height = 1800
        frames = 10
        strips = 6
        size = 0.5
        self.physics_data[port] = Sprite(x=100, y=800 + height // strips * size, width=width // frames,
                                         height=height // strips, player=True, size=size, draw=False, physics=True,
                                         name="Frank")
        if len(self.physics_data) == 1:
            self.it = self.physics_data[port]
        elif len(self.physics_data) == 2:
            self.runner = self.physics_data[port]

    def update(self, list_of_updates):
        self.physics.step()
        if self.runner and self.goal:
            if self.runner.shape.bb.intersects(self.goal.shape.bb):
                self.won = True
        if self.runner and self.it:
            if self.runner.shape.bb.intersects(self.it.shape.bb):
                self.tagged = True
        for port in self.physics_data:
            self.physics_data[port].update()
        for update in list_of_updates:
            port = update[1]
            # self.players[port] = data(x,y,a,p))
            if update[0] == "j":
                if not self.physics_data[port].jumping:
                    self.physics_data[port].jump()
            elif update[0] == "l":
                self.physics_data[port].walk_left()
            elif update[0] == "r":
                self.physics_data[port].walk_right()
            elif update[0] == "h":
                self.physics_data[port].hide()

    def run(self):
        self.make_objects()
        print("Now ready!")
        try:
            while True:
                list_of_updates = self.get_messages()
                self.update(list_of_updates)
                self.send_messages(1 if self.won else (2 if self.tagged else 0))
                time.sleep(1/60)

        except KeyboardInterrupt:
            print("Quitting...")


if __name__ == "__main__":
    global_vars.server = Server()
    global_vars.server.run()
