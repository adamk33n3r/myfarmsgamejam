import sys

import pygame
from pygame.locals import *
pygame.init()

def create_screen(width, height):
    return pygame.display.set_mode((width, height))

def load_image(image_path):
    return pygame.image.load(image_path).convert()

class GameObject:
    def __init__(self, image, height, speed):
        self.speed = speed
        self.image = image
        self.pos = image.get_rect().move(0, height)

    def move(self):
        self.pos = self.pos.move(0, self.speed)
        if self.pos.right > 600:
            self.pos.left = 0

screen = create_screen(640, 480)

background = load_image("Josh Sched.jpg")
screen.blit(background, (0,0))
objects  = [GameObject(background,40,1)]

def handle_input():
    for event in pygame.event.get():
        if event.type in (QUIT, KEYDOWN):
            pygame.display.quit()
            sys.exit()

def update_and_render():
    for o in objects:
        #screen.blit(background, o.pos, o.pos)
        o.move()
        screen.blit(o.image, o.pos)

running=True
def loop():
    while running:
        handle_input()
        update_and_render()
        pygame.display.update()
        pygame.time.delay(100)

if __name__ == "__main__":
    loop()
