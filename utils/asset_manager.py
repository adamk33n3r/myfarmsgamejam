__author__ = 'Adam Keenan'

import pygame

# Axol's colors: 250, 241, 89 and 241, 229, 64

class Manager(object):
    def __init__(self):
        self.sounds = {}
        self.images = {}

    def load_image(self, image_path):
        print("Loading {}".format(image_path))
        name = image_path[image_path.rfind("/")+1:image_path.rfind(".")]
        self.images[name] = pygame.image.load(image_path).convert_alpha()

    def load_sound(self, sound_path):
        print("Loading {}".format(sound_path))
        name = sound_path[sound_path.rfind("/")+1:sound_path.rfind(".")]
        self.sounds[name] = pygame.mixer.Sound(file=sound_path)