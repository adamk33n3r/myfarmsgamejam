__author__ = 'Adam Keenan'

import pygame
from utils import global_vars

class Display(object):
    def __init__(self, title, fullscreen=False, width=global_vars.SCREEN_WIDTH, height=global_vars.SCREEN_HEIGHT):
        pygame.init()
        pygame.display.set_caption(title)
        info=pygame.display.Info()
        print("Current resolution: {}x{}".format(info.current_w,info.current_h))
        if not global_vars.OVERRIDE_CURRENT_RES or fullscreen:
            global_vars.SCREEN_WIDTH, global_vars.SCREEN_HEIGHT = info.current_w, info.current_h
        self.screen = pygame.display.set_mode((global_vars.SCREEN_WIDTH, global_vars.SCREEN_HEIGHT), pygame.FULLSCREEN if fullscreen else 0)
        self.camera_x = 0
        self.camera_y = 0

    def update(self):
        if self.camera_x < 0:
            self.camera_x = 0
        if self.camera_y < 0:
            self.camera_y = 0
        self.screen.flip()

    def clear(self):
        self.screen.fill(pygame.color.THECOLORS["grey"])
