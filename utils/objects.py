__author__ = 'Adam Keenan'

from collections import OrderedDict

import pygame

from utils.gfx.image import SpriteSheet, SpriteStripAnim
from utils import global_vars


class Object(pygame.sprite.Sprite):
    def __init__(self, x, y, width=None, height=None, static=False, image=None, player=False, physics=False):
        super().__init__()
        if image is not None:  # Basically if its not a sprite
            self.image = global_vars.am.images[image]
            self.rect = self.image.get_rect()
            self.rect.x = x
            self.rect.y = y
        else:
            if self.__class__.__name__ != "Sprite":
                self.image = None
            self.rect = pygame.Rect(x, y, width, height)
        if physics:
            if static:
                self.shape = global_vars.server.physics.add_static_box(x, y, self.rect.w, self.rect.h)
            else:
                self.shape = global_vars.server.physics.add_box(self.rect.left, self.rect.top, self.rect.w, self.rect.h)
        self.player = player

    def update_client(self):
        pass

    def draw_client(self, display):
        if self.image is not None:
            if self.player:
                display.screen.blit(self.image,
                                    (global_vars.SCREEN_WIDTH // 2, global_vars.SCREEN_HEIGHT // 3 * 2 + 13))
            else:
                # TODO: WHY IS THIS SO MESSED UP? I SHOULDNT HAVE TO DRAW IN RELATION TO MY HEIGHT AND WIDTH. Look at the physics. something is wrong there
                display.screen.blit(self.image,
                                    (global_vars.SCREEN_WIDTH // 2 + self.rect.x - global_vars.game.player.rect.x,
                                     global_vars.SCREEN_HEIGHT // 3 * 2 - self.rect.y + global_vars.game.player.rect.y + (
                                     13 if self.__class__.__name__ == "Sprite" else 0)))
        if global_vars.DEBUG:
            pygame.draw.circle(display.screen, pygame.color.THECOLORS["blue"],
                               (int(global_vars.SCREEN_WIDTH // 2 + self.rect.x - global_vars.game.player.rect.x),
                                int(global_vars.SCREEN_HEIGHT // 3 * 2 - self.rect.y + global_vars.game.player.rect.y)),
                               2)
            pygame.draw.circle(display.screen, pygame.color.THECOLORS["yellow"],
                               (int(
                                   global_vars.SCREEN_WIDTH // 2 + self.rect.x + self.rect.w - global_vars.game.player.rect.x),
                                int(global_vars.SCREEN_HEIGHT // 3 * 2 - self.rect.y + global_vars.game.player.rect.y)),
                               2)
            pygame.draw.circle(display.screen, pygame.color.THECOLORS["green"],
                               (int(global_vars.SCREEN_WIDTH // 2 + self.rect.x - global_vars.game.player.rect.x), int(
                                   global_vars.SCREEN_HEIGHT // 3 * 2 - self.rect.y + self.rect.h + global_vars.game.player.rect.y)),
                               2)
            pygame.draw.circle(display.screen, pygame.color.THECOLORS["red"],
                               (int(
                                   global_vars.SCREEN_WIDTH // 2 + self.rect.x + self.rect.w - global_vars.game.player.rect.x),
                                int(
                                    global_vars.SCREEN_HEIGHT // 3 * 2 - self.rect.y + self.rect.h + global_vars.game.player.rect.y)),
                               2)

    def draw(self, display):
        raise DeprecationWarning


class Sprite(Object):
    def __init__(self, x, y, width, height, image=None, points=None, counts=None, frames=None, subloops=None,
                 player=False, size=1.0, draw=True, physics=False, name="default"):
        """
        @param width: How wide each frame is
        @param height: How tall each frame is
        @param points: Points corresponding to the start of the animation sequence on the spritesheet
        @param counts: How many frames there are
        @param frames: How many times to repeat each frame
        @param subloops: Point to loop from e.x. 1->2->3->4->5->6->4->5->6->4->5->6
        """
        super().__init__(x=x, y=y, image=None, width=int(width * size), height=int(height * size), player=player,
                         physics=physics)
        self.name = name
        self.current_animation = "jump-right"
        if draw:
            self.sheet = SpriteSheet(image)
            # self.strips = self.sheet.load_strip((0, 0, 624, 150), 6)
            # self.strips = [
            #     utils.gfx.image.SpriteStripAnim(image_path, (0, 0, 104, 150), 6, loop=True, frames=10),
            #     utils.gfx.image.SpriteStripAnim(image_path, (104, 150, 104, 150), 6, loop=True, frames=10)
            # ]
            self.strips = {}
            if frames is None:
                frames = [5] * len(counts)
            if subloops is None:
                subloops = [0] * len(counts)
            for strip, count, frame, subloop in zip(points, counts, frames, subloops):
                self.strips[strip] = SpriteStripAnim(image, points[strip] + (width, height), count, loop=True,
                                                     frames=frame, subloop=subloop,
                                                     resize=(int(width * size), int(height * size)))

            self.strips[self.current_animation].iter()
            self.image = self.strips[self.current_animation].next()

        self.jumping = True
        self.hiding = False
        self.time_at_last_hide = 0

    def change_to(self, animation, update_image=False):
        if self.current_animation != animation:
            self.current_animation = animation
            if update_image:
                self.strips[self.current_animation].iter()
                self.image = self.strips[self.current_animation].next()

    def update_client(self, update):
        if update:
            self.rect.x, self.rect.y = float(update[0]), float(update[1])
            self.change_to(update[2], True)
            self.time_at_last_hide = float(update[3])
        self.image = self.strips[self.current_animation].next()

        if self.hiding and self.time_at_last_hide < (pygame.time.get_ticks() / 1000 - 5):
            self.hide()
            self.time_at_last_hide = pygame.time.get_ticks() / 1000

    def update(self):
        velocity = self.shape.body.velocity
        # Checks to see if you've hit the ground after a jump
        if self.jumping:
            if int(velocity.y) == 0:
                self.jumping = False
                if "jump-" in self.current_animation:
                    self.change_to(self.current_animation[self.current_animation.find("-") + 1:])
        elif velocity.y < -0.1:
            self.jump(True)
        elif "stand" not in self.current_animation and int(velocity.x) == 0:
            self.change_to("stand-" + self.current_animation)

        # Limits speed
        if velocity.y > 500:
            velocity.y = 500
        elif velocity.y < -2000:
            velocity.y = -2000
        if velocity.x > 512:
            velocity.x = 512
        elif velocity.x < -512:
            velocity.x = -512
        # Slows you down
        if velocity.x != 0 and not self.jumping:
            velocity.x = velocity.x - 10 * (velocity.x / abs(velocity.x)) if abs(velocity.x) > 10 else 0

        if self.hiding and self.time_at_last_hide < (pygame.time.get_ticks() / 1000 - 5):
            self.hide()
            self.time_at_last_hide = pygame.time.get_ticks() / 1000

    def jump(self, no_impulse=False):
        self.jumping = True
        self.change_to(
            "jump-" + self.current_animation if "-" not in self.current_animation else self.current_animation[
                                                                                       self.current_animation.find(
                                                                                           "-") + 1:])
        if not no_impulse:
            self.apply_impulse((0, 500))

    def walk(self, impulse):
        if impulse[0] > 0:
            self.walk_right(impulse)
        else:
            self.walk_left(impulse)

    def walk_right(self, impulse=(100, 0)):
        if not self.hiding:
            if not self.jumping:
                self.change_to("right")
            self.apply_impulse(impulse)

    def walk_left(self, impulse=(-100, 0)):
        if not self.hiding:
            if not self.jumping:
                self.change_to("left")
            self.apply_impulse(impulse)

    def hide(self):
        if self.hiding:
            self.change_to(self.current_animation.replace("invisible-", ""))
            self.hiding = False
            self.time_at_last_hide = pygame.time.get_ticks() / 1000
        else:
            if self.time_at_last_hide < (pygame.time.get_ticks() / 1000 - 15):
                self.change_to("stand-invisible-" + self.current_animation[self.current_animation.rfind("-") + 1:])
                self.hiding = True
                self.time_at_last_hide = pygame.time.get_ticks() / 1000

    def apply_impulse(self, impulse):
        global_vars.game.physics.apply_impulse(self, impulse)


def gen_sprite(player=True):
    if "axol" not in global_vars.am.images:
        global_vars.am.load_image("assets/sprites/axol.png")
    width = 2000
    height = 1800
    frames = 10
    strips = 6
    size = 0.5
    sprite = Sprite(100, 1000 + height // strips * size, width // frames, height // strips,
                    image="axol", points=OrderedDict(
            (("left", (0, 0)),
             ("right", (0, height // strips)),
             ("jump-left", (0, height // strips * 2)),
             ("jump-right", (0, height // strips * 3)),
             ("stand-left", (0, height // strips * 4)),
             ("stand-right", (0, height // strips * 5)),
             ("stand-invisible-left", (width // frames * 8, 0)),
             ("stand-invisible-right", (width // frames * 9, 0)),
            )), counts=(8, 8, 10, 10, 10, 10, 1, 1), frames=(2, 2, 3, 3, 3, 3, 1, 1), subloops=(0,0,7,7,0,0,0,0), player=player, size=size)
    return sprite