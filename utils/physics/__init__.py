__author__ = 'Adam Keenan'

import pymunk
from utils import global_vars

class Physics(object):
    def __init__(self, time):
        self.space = pymunk.Space()
        self.space.gravity = (0.0, -900.0)
        self.time = time

    def step(self):
        self.space.step(self.time)

    def add_box(self, x, y, width, height):
        mass = 1
        inertia = pymunk.inf
        body = pymunk.Body(mass, inertia)
        body.position = x, y
        shape = pymunk.Poly.create_box(body, size=(width, height))
        self.space.add(body, shape)
        return shape

    def add_static_box(self, x, y, width, height):
        """
        Remember these are drawn from the center
        """
        body = self.space.static_body
        body.position = x, y
        shape = pymunk.Poly.create_box(body, size=(width, height))
        # shape.friction = 1000
        self.space.add(shape)
        return shape

    def add_ball(self, x, y, radius):
        mass = 1
        inertia = pymunk.moment_for_circle(mass, 0, radius)
        body = pymunk.Body(mass, inertia)
        body.position = x, y
        shape = pymunk.Circle(body, radius)
        self.space.add(body, shape)
        return shape

    def add_static_L(self):
        body = pymunk.Body()
        body.position = 300, 300
        l1 = pymunk.Segment(body, (-150, 0), (255, 0), 5)
        l2 = pymunk.Segment(body, (-150, 0), (-150, 50), 5)
        # For static bodies, don't and the 'body' to the space. Only the shapes. So it doesn't move.
        self.space.add(l1, l2)
        return l1, l2

    def add_L(self):
        rotation_center_body = pymunk.Body()
        rotation_center_body.position = (300, 300)

        rotation_limit_body = pymunk.Body()
        rotation_limit_body.position = 200, 300

        body = pymunk.Body(10, 10000)
        body.position = 300, 300
        l1 = pymunk.Segment(body, (-150, 0), (255, 0), 5)
        l2 = pymunk.Segment(body, (-150, 0), (-150, 50), 5)

        rotation_center_joint = pymunk.PivotJoint(body, rotation_center_body, (0, 0), (0, 0))
        joint_limit = 25
        rotation_limit_joint = pymunk.SlideJoint(body, rotation_limit_body, (-100, 0), (0, 0), 0, joint_limit)

        self.space.add(l1, l2, body, rotation_center_joint, rotation_limit_joint)
        return l1, l2

    def apply_impulse(self, sprite, impulse):
        sprite.shape.body.apply_impulse(impulse,(0,0))
        # sprite.shape.body.velocity.x+=impulse[0]
        # sprite.shape.body.velocity.y+=impulse[1]
        # sprite.shape.body.position.y+=50