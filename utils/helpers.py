__author__ = 'Adam Keenan'
from utils import global_vars


def to_pygame_point(p):
    return int(p.x), int(-p.y + global_vars.SCREEN_HEIGHT)

def to_pygame_y(y):
    return int(global_vars.SCREEN_HEIGHT - y)