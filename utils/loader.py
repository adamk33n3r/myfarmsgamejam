__author__ = 'Adam Keenan'

from utils.objects import Object
from utils import global_vars

def load_server(filename):
    file = open(filename)
    lst=[]
    for line in file:
        if line[0] == "#" or line[0] == '\n':
            continue
        x, y = line.split(", ")
        lst.append(Object(int(x)+100//2, int(y)+150//2, 200, 20, True, physics=True))
    return lst

def load_client(filename):
    file = open(filename)
    for line in file:
        if line[0] == "#" or line[0] == '\n':
            continue
        x, y = line.split(", ")
        global_vars.game.add_entity(Object(int(x), int(y), static=True, image="platform"))
