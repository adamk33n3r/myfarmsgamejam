__author__ = 'Adam Keenan'

import pygame

from utils.input import controller, keyboard
from utils import global_vars, display
from utils.gfx import text
from utils.objects import gen_sprite
from utils.physics import Physics
from utils.gfx.image import Image

from utils.network.client import Client


class Game(object):
    def __init__(self, title, address):
        if address:
            self.client = Client(address)
        else:
            self.client = Client()

        pygame.mixer.pre_init(frequency=44100, size=-16, channels=2, buffer=4096)
        self.RUNNING = True
        self.images = []
        self.entities = pygame.sprite.Group()
        self.display = display.Display(title, global_vars.FULLSCREEN)
        self.clock = pygame.time.Clock()
        pygame.key.set_repeat(1, 50)

        self.player = None
        self.others = {}

        self.bgmusic = None

        self.it = False
        self.endgame=""

        self.goal = None

        pygame.event.set_allowed(None)
        pygame.event.set_allowed([pygame.QUIT, pygame.KEYDOWN])

        if controller.get_count() > 0:
            global_vars.USING_CONTROLLERS = True
            controller.initialize_controllers()
            print("Detected {} controllers.".format(controller.get_count()))
        else:
            print("No controllers detected. Using keyboard.")


    def loop(self):
        if not global_vars.MUTE:
            self.bgmusic.set_volume(.3)
            self.bgmusic.play(-1)
        while self.RUNNING:
            self.handle_input(global_vars.USING_CONTROLLERS)
            updates=self.client.get_messages()
            self.update(updates)
            self.draw()
            self.clock.tick(60)
        print("FPS: ",self.clock.get_fps())
        self.client.quit()
        pygame.display.quit()


    def handle_input(self, using_controller):
        for event in pygame.event.get():
            if event.type == pygame.QUIT or event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                self.quit()
            if not using_controller and event.type == pygame.KEYDOWN:
                # if event.key == pygame.K_SPACE:
                #     if not self.players[0].jumping:
                #         self.players[0].jump()
                # elif event.key == pygame.K_LEFT:
                #     self.players[0].walk_left()
                # elif event.key == pygame.K_RIGHT:
                #     self.players[0].walk_right()
                # elif event.key == pygame.K_RCTRL:
                #     self.players[0].hide()
                # else:
                #     print("key: ", pygame.key.name(event.key))
                if event.key == pygame.K_SPACE:
                    self.client.send_command("ij")
                elif event.key == pygame.K_LEFT:
                    self.client.send_command("il")
                elif event.key == pygame.K_RIGHT:
                    self.client.send_command("ir")
                elif event.key == pygame.K_RCTRL:
                    self.client.send_command("ih")
                else:
                    # print("key: ", pygame.key.name(event.key))
                    # Only lets one KEYDOWN happen
                    pygame.event.clear(pygame.KEYDOWN)
        # if using_controller:
        #     controller.gather_input()
        #     cont1 = controller.VALUES[0]
        #     if not self.players[0].jumping and cont1["BUTTONS"][controller.BUTTONMAPPINGS["A"]]:
        #         self.players[0].jump()
        #     if cont1["BUTTONS"][controller.BUTTONMAPPINGS["Y"]]:
        #         self.quit()
        #     if cont1["AXES"][controller.AXESMAPPINGS["LX"]]:
        #         val = cont1["AXES"][controller.AXESMAPPINGS["LX"]]
        #         self.players[0].walk((30 * val, 0))
        #     if cont1["AXES"][controller.AXESMAPPINGS["LY"]]:
        #         val = cont1["AXES"][controller.AXESMAPPINGS["LY"]]
        #         if self.players[0].jumping:
        #             if val > 0.95:
        #                 self.players[0].apply_impulse((0, -100))
        #         else:
        #             if val == -1:
        #                 self.players[0].jump()
        # else:
        #     keyboard.gather_input()

    # Graphics stuff
    def add_image(self, image):
        self.images.append(image)

    def add_entity(self, entity):
        self.entities.add(entity)

    def add_player(self, sprite):
        if not self.player:
            self.player = sprite
        # self.entities.add(sprite)

    def update(self, updates):
        # for sprite in self.entities.sprites():
        #     sprite.update_client()
        if not updates: return
        for update in updates:
            port = int(update[-1])
            if not self.goal:
                goal_x, goal_y = float(update[4]),float(update[5])
                self.goal = Image(goal_x, goal_y, static=True, image="goal_post_test")
                global_vars.game.add_image(self.goal)
            if port == self.client.clientport:
                if bool(update[6]) == 1:
                    self.it = True
                if int(update[-2]) == 1:
                    self.endgame=("You got the GOALLL!!!")
                elif int(update[-2]) == 2:
                    self.endgame=("You got tagged!")
                self.player.update_client(update)
            else:
                if port not in self.others:
                    self.others[port] = gen_sprite(False)
                if int(update[-2]) == 1:
                    self.endgame=("They got the GOALLL!!!")
                elif int(update[-2]) == 2:
                    self.endgame=("They got tagged!")
                self.others[port].update_client(update)
        if not updates:
            self.player.update_client(None)

    def draw(self):
        self.display.clear()

        for image in self.images:
            image.draw(self.display)
        self.player.draw_client(self.display)
        for other in self.others:
            self.others[other].draw_client(self.display)
        # sprites_to_remove = []
        for sprite in self.entities.sprites():
            # print("Removing a " + sprite.__class__.__name__)
            # if sprite.shape.body.position.y < 0:
            # sprites_to_remove.append(sprite)
            sprite.draw_client(self.display)
        # for sprite in sprites_to_remove:
        #     self.physics.space.remove(sprite.shape, sprite.shape.body)
        #     self.entities.remove(sprite)

        text.draw(self.display.screen, "FPS: " + str(int(self.clock.get_fps())), 18, 0, 0)
        text.draw(self.display.screen, "Using controllers: " + str(global_vars.USING_CONTROLLERS), 18, 0, 14)
        t = int(self.player.time_at_last_hide - pygame.time.get_ticks() / 1000 + 16)
        text.draw(self.display.screen, "Time until can hide again: " + str(t if t >= 0 else 0), 18, 0, 28)
        text.draw(self.display.screen, "You are it: " + str(self.it), 18, 0, 42)
        if self.endgame:
            text.draw(self.display.screen, self.endgame, 28, global_vars.SCREEN_WIDTH//4, global_vars.SCREEN_HEIGHT//2)
        pygame.display.flip()

    def quit(self):
        print("Quitting...")
        self.RUNNING = False
