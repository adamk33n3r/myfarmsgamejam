__author__ = 'Adam Keenan'

import pygame.freetype

pygame.freetype.init()
font = pygame.freetype.SysFont("Arial", 12)


def draw(screen, text, size, x, y):
    font.render_to(screen, (x, y), str(text), pygame.color.THECOLORS["yellow"], size=size)