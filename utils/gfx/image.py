__author__ = 'Adam Keenan'

import pygame
from utils import global_vars
from utils.global_vars import am

class Image(object):
    def __init__(self, x, y, image, static=False):
        self.image = global_vars.am.images[image]
        # self.image = pygame.transform.scale(self.image, (1000,773))
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.static = static
        if self.static:
            self.player = global_vars.game.player

    def update(self):
        pass

    def draw(self, display):
        if self.static:
            # (global_vars.SCREEN_WIDTH // 2 + self.shape.bb.left - global_vars.game.players[0].shape.bb.left,    global_vars.SCREEN_HEIGHT // 3 * 2 - self.shape.bb.top + global_vars.game.players[0].shape.bb.top)
            cur_pos = self.player.rect
            # size=[cur_pos.left-global_vars.SCREEN_WIDTH//2,cur_pos.top-global_vars.SCREEN_HEIGHT//2,cur_pos.left + global_vars.SCREEN_WIDTH//2,global_vars.SCREEN_HEIGHT]
            # size=[cur_pos.left-50,cur_pos.top-50,cur_pos.right +50,cur_pos.bottom+50]
            # size=[0,-1700,6000,4000]
            size=None
            # for i in range(len(size)):
            #     if size[i] < 0:
            #         size[i] = 0
            display.screen.blit(self.image, (
                global_vars.SCREEN_WIDTH // 2 + self.rect.left - cur_pos.x,
                global_vars.SCREEN_HEIGHT // 3 * 2 - self.rect.top + cur_pos.y),size)
        else:
            display.screen.blit(self.image, self.rect)
        if global_vars.DEBUG:
            pygame.draw.circle(display.screen, pygame.color.THECOLORS["blue"],
                               (int(global_vars.SCREEN_WIDTH // 2 + self.rect.x - global_vars.game.player.rect.x), int(global_vars.SCREEN_HEIGHT // 3 * 2 - self.rect.y + global_vars.game.player.rect.y)), 2)
            pygame.draw.circle(display.screen, pygame.color.THECOLORS["yellow"],
                               (int(global_vars.SCREEN_WIDTH // 2 + self.rect.x+self.rect.w - global_vars.game.player.rect.x), int(global_vars.SCREEN_HEIGHT // 3 * 2 - self.rect.y + global_vars.game.player.rect.y)), 2)
            pygame.draw.circle(display.screen, pygame.color.THECOLORS["green"],
                               (int(global_vars.SCREEN_WIDTH // 2 + self.rect.x - global_vars.game.player.rect.x), int(global_vars.SCREEN_HEIGHT // 3 * 2 - self.rect.y+self.rect.h + global_vars.game.player.rect.y)), 2)
            pygame.draw.circle(display.screen, pygame.color.THECOLORS["red"],
                               (int(global_vars.SCREEN_WIDTH // 2 + self.rect.x+self.rect.w - global_vars.game.player.rect.x), int(global_vars.SCREEN_HEIGHT // 3 * 2 - self.rect.y+self.rect.h + global_vars.game.player.rect.y)), 2)


class SpriteSheet(object):
    """
    Got this from http://www.pygame.org/wiki/Spritesheet?parent=CookBook and edited slightly to my needs
    """

    def __init__(self, filename):
        self.sheet = global_vars.am.images[filename]

    # Load a specific image from a specific rectangle
    def image_at(self, rectangle, colorkey=None):
        "Loads image from x,y,x+offset,y+offset"
        rect = pygame.Rect(rectangle)
        image = pygame.Surface(rect.size, flags=pygame.SRCALPHA).convert_alpha()
        image.blit(self.sheet, (0, 0), rect)
        if colorkey is not None:
            if colorkey is -1:
                colorkey = image.get_at((0, 0))
            image.set_colorkey(colorkey, pygame.RLEACCEL)
        return image

    # Load a whole bunch of images and return them as a list
    def images_at(self, rects, colorkey=None):
        "Loads multiple images, supply a list of coordinates"
        return [self.image_at(rect, colorkey) for rect in rects]

    # Load a whole strip of images
    def load_strip(self, rect, image_count, colorkey=None):
        "Loads a strip of images and returns them as a list"
        tups = [(rect[0] + rect[2] * x, rect[1], rect[2], rect[3])
                for x in range(image_count)]
        return self.images_at(tups, colorkey)


class SpriteStripAnim(object):
    """
    Got this from http://www.pygame.org/wiki/Spritesheet?parent=CookBook and edited slightly to my needs

    sprite strip animator

    This class provides an iterator (iter() and next() methods), and a
    __add__() method for joining strips which comes in handy when a
    strip wraps to the next row.
    """

    def __init__(self, filename, rect, count, colorkey=None, loop=False, subloop=0, frames=1, resize=None):
        """construct a SpriteStripAnim

        filename, rect, count, and colorkey are the same arguments used
        by spritesheet.load_strip.

        loop is a boolean that, when True, causes the next() method to
        loop. If False, the terminal case raises StopIteration.

        frames is the number of ticks to return the same image before
        the iterator advances to the next image.
        """
        self.filename = filename
        ss = SpriteSheet(filename)
        self.images = ss.load_strip(rect, count, colorkey)
        resized_images = []
        if resize is not None:
            for image in self.images:
                resized_images.append(pygame.transform.scale(image, resize))
            self.images = resized_images
        self.i = 0
        self.loop = loop
        self.subloop = subloop
        self.frames = frames
        self.f = frames

    def iter(self):
        self.i = 0
        self.f = self.frames
        return self

    def next(self):
        if self.i >= len(self.images):
            if not self.loop:
                raise StopIteration
            else:
                self.i = self.subloop
        image = self.images[self.i]
        self.f -= 1
        if self.f == 0:
            self.i += 1
            self.f = self.frames
        return image

    def prev(self):
        if self.i < 0:
            if not self.loop:
                raise StopIteration
            else:
                self.i = len(self.images)
        image = self.images[self.i]
        self.f -= 1
        if self.f == 0:
            self.i -= 1
            self.f = self.frames
        return image

    def __add__(self, ss):
        self.images.extend(ss.images)
        return self