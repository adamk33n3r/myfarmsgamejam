__author__ = 'Adam Keenan'

import pygame

from utils import global_vars
import utils.gfx.image


class Entity(pygame.sprite.Sprite):
    def __init__(self, x, y, width, height, color=pygame.color.THECOLORS["white"]):
        super().__init__()

        self.image = pygame.Surface((width, height))
        self.image.fill(color)

        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y

        self.shape = None

    def draw(self, display):
        pass