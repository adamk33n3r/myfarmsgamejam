__author__ = 'Adam Keenan'

import pygame
from utils.helpers import to_pygame_point
from utils import global_vars
from utils.gfx import Entity


class Shape(Entity):
    def __init__(self, x, y, width, height, color=pygame.color.THECOLORS["white"]):
        super().__init__(x, y, width, height, color)


class Square(Shape):
    def __init__(self, x, y, size=5, color=pygame.color.THECOLORS["white"]):
        super().__init__(x, y, size, size, color)


class Circle(Shape):
    def __init__(self, x, y, radius=5, color=pygame.color.THECOLORS["white"]):
        super().__init__(x, y, radius, radius, color)
        self.shape = global_vars.game.physics.add_ball(x, y, radius)

    def draw(self, display):
        p = int(self.shape.body.position.x), global_vars.SCREEN_HEIGHT - int(self.shape.body.position.y)
        pygame.draw.circle(display.screen, pygame.color.THECOLORS["red"], p, int(self.shape.radius))


class L(Shape):
    def __init__(self, color=pygame.color.THECOLORS["white"]):
        super().__init__(0, 0, 300, 300)
        self.shape, self.shape2 = global_vars.game.physics.add_L()

    def draw(self, display):
        for line in [self.shape, self.shape2]:
            body = line.body
            pv1 = body.position + line.a.rotated(body.angle)  #rotation not needed if truly static
            pv2 = body.position + line.b.rotated(body.angle)
            p1 = to_pygame_point(pv1)
            p2 = to_pygame_point(pv2)
            pygame.draw.lines(display.screen, pygame.color.THECOLORS["green"], False, [p1, p2])