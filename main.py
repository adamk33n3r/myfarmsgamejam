__author__ = 'Adam Keenan'
import sys

import pygame
from pygame import mixer

from utils.game import Game
from utils.objects import Object, gen_sprite
from utils.gfx.image import Image
from utils import global_vars, loader
from utils.network.server import Server
from utils.asset_manager import Manager

if __name__ == "__main__":
    if len(sys.argv) >= 2:
        #print("Starting {}".format(sys.argv[1]))
        if sys.argv[1] == "server":
            pygame.init()
            global_vars.server = Server()
            global_vars.game = global_vars.server
            global_vars.server.run()
            sys.exit(0)
        #elif sys.argv[1] != "client":
        #    print("Use either client or server")
        #    sys.exit(1)
    if len(sys.argv) > 2:
        address = sys.argv[2]
    else:
        address = None
    global_vars.game = Game("MyFarms GameJam Early 2014 - Adam Keenan & Justice Juraschek", address)
    global_vars.am = Manager()
    global_vars.am.load_image("assets/enviro/scene/layer1.png")
    global_vars.am.load_image("assets/enviro/scene/layer2.png")
    global_vars.am.load_image("assets/enviro/scene/layer3.png")
    sprite = gen_sprite()
    global_vars.game.add_player(sprite)
    global_vars.game.bgmusic = mixer.Sound(file="assets/music/go_cart_loop_mix.wav")
    global_vars.game.add_image(
        Image(0, 3091, static=True, image="layer1"))
    global_vars.game.add_image(
        Image(0, 3091, static=True, image="layer2"))
    global_vars.game.add_image(
        Image(0, 3091, static=True, image="layer3"))

    global_vars.am.load_image(image_path="assets/enviro/platforms/platform.png")
    global_vars.am.load_image("assets/enviro/goal_post_test.png")

    # global_vars.game.add_entity(Object(1250, 600, static=True, image="platform"))
    # global_vars.game.add_entity(Object(1350, 500, static=True, image="platform"))
    # global_vars.game.add_entity(Object(1450, 400, static=True, image="platform"))
    # global_vars.game.add_entity(Object(1550, 300, static=True, image="platform"))
    # global_vars.game.add_entity(Object(500, 800, static=True, image="platform"))
    # global_vars.game.add_entity(Object(300, 800, static=True, image="platform"))
    # global_vars.game.add_entity(Object(100, 820, static=True, image="platform"))
    # global_vars.game.add_entity(Object(0, 0, static=True, image="platform"))

    loader.load_client("assets/enviro/platform_list.txt")
    global_vars.game.loop()
