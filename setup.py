# You can call this script with different commands.
# - build_exe to create an exe
# - bdist_msi to create a simple installer for Windows
# - bdist_mac to build an application bundle for Mac
# - bdist_dmg to build an application bundle then puts it in a disk image for easy distribution
application_title = "MyFarmsGameJam-Keenan-Juraschek.exe"
version = "0.3"
main_python_file = "main.py"
include_files = ["assets"]

import sys
if sys.platform == "darwin":
    include_files.append(("lib/libchipmunk.dylib", ""))
elif sys.platform == "win32":
    include_files.append(("lib\\chipmunk.dll",""))
compress = False
icon = None
icon_file = "artwork/icons/game.icns"
shortcut_name = None

from cx_Freeze import setup, Executable

setup(
        name = application_title,
        version = version,
        description = "The MyFarms GameJam 2014 submission by Adam Keenan and Justice Juraschek",
        options = {"build_exe":{"include_files":include_files},"bdist_mac":{"iconfile":icon_file}},
        executables = [Executable(script=main_python_file, targetName=application_title, compress=compress, icon=icon,shortcutName=shortcut_name)]
        )
#from subprocess import call
#
#if sys.platform == "darwin":
#    call(["cp","/usr/local/lib/python3.3/site-packages/pymunk/libchipmunk.dylib","build/{}-{}.app/Contents/MacOS/".format(application_title, version)])
#    call(["cp","-r","assets","build/{}-{}.app/Contents/MacOS/".format(application_title, version)])
#elif sys.platform == "win32":
#    call(["copy","C:\\Python3.3\\Lib\\site-packages\\pymunk\\chipmunk.dll","\\build\\"])
